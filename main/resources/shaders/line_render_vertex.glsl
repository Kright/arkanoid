#version 460

uniform mat4 uMatrix;

layout(location = 0) in vec2 aPos;
layout(location = 1) in vec4 aColor;

layout(location = 0) out vec4 vColor;

void main() {
    gl_Position = uMatrix * vec4(aPos.xy, 0.0, 1.0);
    vColor = aColor;
};
