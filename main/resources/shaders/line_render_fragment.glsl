#version 460

layout(location = 0) in vec4 vColor;

void main() {
    gl_FragColor = vColor;
};
