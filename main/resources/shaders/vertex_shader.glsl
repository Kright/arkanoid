#version 460

uniform mat4 uMatrix;

in vec3 aColor;
in vec2 aPos;

out vec3 vColor;

void main() {
    gl_Position = uMatrix * vec4(aPos.xy, 0.0, 1.0);
    vColor = aColor;
};
