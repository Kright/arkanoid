 
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <mygl.h>

#include <glfw_wrappers/glfwinit.hpp>
#include <glfw_wrappers/window.hpp>
#include <glfw_wrappers/shaders.hpp>
#include <glfw_wrappers/buffer.hpp>
#include <util/resources.hpp>
#include <util/math.hpp>
#include <game/platform.hpp>
#include <lines_render.hpp>

#include "glm/ext.hpp"
 
static const struct
{
    float x, y;
    float r, g, b;
} vertices[3] =
{
    { -0.6f, -0.4f, 1.f, 0.f, 0.f },
    {  0.6f, -0.4f, 0.f, 1.f, 0.f },
    {   0.f,  0.6f, 0.f, 0.f, 1.f }
};
 
static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    std::cout << "keyCallback(key = " << key << ", scancode = " << scancode << ", action " << action << ", mods" << mods << std::endl;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

static void errorCallback(int error, const char* description) {
    std::cerr << "ERROR " << error << ": " << description << std::endl; 
}

glm::mat4x4 makeSquareProjection(Vector2i size) {
    glm::mat4x4 result(static_cast<glm::mat4x4>(1));
    if (size.x > size.y) {
        result[0][0] *= static_cast<float>(size.y) / size.x;
    } else {
        result[1][1] *= static_cast<float>(size.x) / size.y;
    }
    return result;
}

vec2 getLocalCursorPos(Window &window, mat4x4 projection) {
    Vector2i windowSize = window.getSize();
    vec2 normPos = window.getCursorPos() * vec2{2.0 / windowSize.x, 2.0 / windowSize.y} - vec2{1, 1};
    mat4x4 projectionInv = glm::inverse(projection);
    vec4 posAsLocal = projectionInv * vec4{normPos.x, normPos.y, 0.0, 1.0};
}

void runGame(Window &window) {
    ArrayBuffer triangleVertices{};
    triangleVertices.bind();
    triangleVertices.setData(vertices, sizeof(vertices), GL_STATIC_DRAW);
    
    ShaderProgram program = ShaderProgram::Compile(
        Resources::loadFileAsString("resources/shaders/vertex_shader.glsl"), 
        Resources::loadFileAsString("resources/shaders/fragment_shader.glsl")
    );

    LinesRender linesRender{};
    Platform platform{PlatformRail{-0.6, -1, 1}, PlatformParams{{0.2, 0.07}, 1.0}};

    const GLint aPos = program.getAttribLocation("aPos");
    const GLint aColor = program.getAttribLocation("aColor");
    
    glEnableVertexAttribArray(aPos);
    glEnableVertexAttribArray(aColor);

    while (!window.isShouldClose()) {
        Vector2i size = window.getFramebufferSize();

        glViewport(0, 0, size.x, size.y);
        glClearColor(0, 0, 0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);
    
        program.use();
        triangleVertices.bind();
        glVertexAttribPointer(aPos, 2, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (void*) 0);
        glVertexAttribPointer(aColor, 3, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (void*) (sizeof(float) * 2));

        glm::mat4x4 projection = makeSquareProjection(size);
        program.setUniform("uMatrix", projection);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        {
            linesRender.setColor({1.0, 0.0, 1.0, 1.0});
            vec2 zero{0, 0};
            vec2 up{0, 1};
            vec2 right{1, 0};
            linesRender.addLine(zero, up);
            linesRender.addLine(up, up + right);
            linesRender.addLine(right, right + up);
            linesRender.addLine(zero, right);

            linesRender.addRect({-0.5, -0.5}, {0.2, 0.1});

            vec2 size = platform.getSize();
            linesRender.addRect(platform.getCenter() - size / vec2{2.0, 2.0} , size);

            linesRender.render(projection);
        }

        vec2 cursorPos = getLocalCursorPos(window, projection);
        platform.update(posAsLocal.x, 1.0 / 60.0);
    
        window.swapBuffers();
        glfwPollEvents();
    }
}

int main(void) {
    try {
        glfwSetErrorCallback(&errorCallback);
        GLFWInit init;
    
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

        Window window({640, 480}, "arkanoid");
        glfwSetKeyCallback(&window, &keyCallback);
    
        glfwMakeContextCurrent(&window);
        gladLoadGL();
        glfwSwapInterval(1);

        runGame(window);
    } catch (const std::runtime_error& ex) {
        std::cout << ex.what() << std::endl;
    } 
}