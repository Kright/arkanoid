#pragma once 

#include <string>
#include <fstream>
#include <sstream>

namespace Resources {
    inline std::string loadFileAsString(const std::string &fileName) {
        std::ifstream f(fileName);
        if (f.fail()) {
            throw std::runtime_error("file \"" + fileName + "\" isn't exist");
        }
        std::stringstream ss;
        ss << f.rdbuf();
        return ss.str();
    }
};
