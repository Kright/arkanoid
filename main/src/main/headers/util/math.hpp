#pragma once

template <class T>
struct Vector2 {
    T x;
    T y;
};

using Vector2i = Vector2<int>;

template<class T> 
constexpr T clamp(T a, T min, T max) {
    return (a < min) ? min : ((a > max) ? max : a);
}
