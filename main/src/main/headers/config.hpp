#pragma once

#include <string>
#include <util/math.hpp>

struct Config {
    bool isFullscreen;
    Vector2i screenSize;
    std::string windowName;

    static inline Config MakeDefault() {
        return {
            false,
            {600, 800},
            "Arcanoid",
        };
    }
};
