#pragma once

#include <vector>
#include <glm/vec4.hpp>
#include <glm/vec2.hpp>

#include <glfw_wrappers/vector_buffer.hpp>
#include <glfw_wrappers/shaders.hpp>
#include <glfw_wrappers/buffer.hpp>
#include <util/resources.hpp>

using glm::vec2;
using glm::vec4;
using glm::mat4;

class LinesRender {
private:
    vec4 currentColor;
    VectorBuffer vectorBuffer;
    ArrayBuffer arrayBuffer;
    ShaderProgram shader = ShaderProgram::Compile(
        Resources::loadFileAsString("resources/shaders/line_render_vertex.glsl"),
        Resources::loadFileAsString("resources/shaders/line_render_fragment.glsl")
    );
    const GLint aPos = shader.getAttribLocation("aPos");
    const GLint aColor = shader.getAttribLocation("aColor");
    static constexpr size_t kFloatsInVertex = 6;

    inline void addPoint(vec2 point) {
        vectorBuffer.push(point);
        vectorBuffer.push(currentColor);
    }

    inline size_t pointsCount() const {
        return vectorBuffer.getData().size() / 6;
    }
public: 
    LinesRender() {
        glEnableVertexAttribArray(aPos);
        glEnableVertexAttribArray(aColor);
    }

    inline void setColor(vec4 color) noexcept{
        currentColor = color;
    }

    inline void addLine(vec2 from, vec2 to) {
        addPoint(from);
        addPoint(to);
    }

    inline void addRect(vec2 point, vec2 size) {
        vec2 right{size.x, 0};
        vec2 up{0, size.y};
        addLine(point, point +right);
        addLine(point + right, point + right + up);
        addLine(point + right + up, point + up);
        addLine(point + up, point);
    }

    inline void render(const mat4& projection) {
        arrayBuffer.bind();
        arrayBuffer.setData(vectorBuffer.getData().data(), vectorBuffer.getData().size(), GL_DYNAMIC_DRAW);

        shader.use();

        glVertexAttribPointer(aPos, 2, GL_FLOAT, GL_FALSE, kFloatsInVertex * sizeof(float), (void*) 0);
        glVertexAttribPointer(aColor, 3, GL_FLOAT, GL_FALSE, kFloatsInVertex * sizeof(float), (void*) (sizeof(float) * 2));

        shader.setUniform("uMatrix", projection);

        glDrawArrays(GL_LINES, 0, pointsCount());

        vectorBuffer.clear();
    }
};
