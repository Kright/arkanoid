#pragma once

#include <stdexcept>

#include <mygl.h>
#include <GLFW/glfw3.h>

struct GLFWInit { 
    inline GLFWInit() {
        if (!glfwInit()) {
            throw std::runtime_error("glfwInit fail");
        }
    }

    inline ~GLFWInit() {
        glfwTerminate();
    }
};
