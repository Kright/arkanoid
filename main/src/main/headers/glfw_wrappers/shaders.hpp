#pragma once 

#include <string>
#include <stdexcept>
#include <vector>
#include <sstream>
#include "mygl.h"
#include <glm/mat4x4.hpp>
#include "unique_val.hpp"


using glm::mat4x4;


struct ShaderDeleter {
    inline void operator()(GLuint id) {
        glDeleteShader(id);
    }
};


struct ShaderProgramDeleter { 
    inline void operator()(GLuint id) {
        glDeleteProgram(id);
    }
};


template<int kShaderType>
class Shader {
    static_assert(kShaderType == GL_VERTEX_SHADER || kShaderType == GL_FRAGMENT_SHADER);
protected:
    UniqueVal<GLuint, ShaderDeleter> id;

    inline explicit Shader(): id(glCreateShader(kShaderType)) {
        if (getId() == 0) {
            throw std::runtime_error("can't create shader");
        }
    };

public:
    inline GLuint getId() const noexcept {
        return id.getValue();
    }

    inline GLint getInfoLogLength() const noexcept {
        GLint length;
        glGetShaderiv(getId(), GL_INFO_LOG_LENGTH, &length);
        return length;
    }

    inline std::string GetInfoLog() {
        size_t length = getInfoLogLength();
        if (length == 0) {
            return "";
        }
        std::string result(length, ' ');
        glGetShaderInfoLog(getId(), result.size(), nullptr, &result[0]);
        return result;
    }

    static inline Shader Compile(const std::string& code) {
        Shader result;
        const char * text = code.c_str(); 
        glShaderSource(result.getId(), 1, &text, NULL);
        glCompileShader(result.getId());

        int errorCode = 0;
        glGetShaderiv(result.getId(), GL_COMPILE_STATUS, &errorCode);
        switch (errorCode) {
            case GL_INVALID_VALUE:
                throw std::runtime_error("can't compile shader, GL_INVALID_VALUE:\n" + code);
            case GL_INVALID_OPERATION:
                throw std::runtime_error("can't compile shader, GL_INVALID_OPERATION:\n" + code);
        }

        return result;
    }
};

using VertexShader = Shader<GL_VERTEX_SHADER>;
using FragmentShader = Shader<GL_FRAGMENT_SHADER>;


class ShaderProgram{
private:
    UniqueVal<GLuint, ShaderProgramDeleter> id;
    VertexShader vertexShader;
    FragmentShader fragmentShader;

public:
    inline ShaderProgram(VertexShader vertexShader, FragmentShader fragmentShader): 
            id(glCreateProgram()),
            vertexShader(std::move(vertexShader)), 
            fragmentShader(std::move(fragmentShader)) {
        glAttachShader(getId(), this->vertexShader.getId());
        glAttachShader(getId(), this->fragmentShader.getId());
        glLinkProgram(getId());

        int linkStatus;
        glGetProgramiv(getId(), GL_LINK_STATUS, &linkStatus);
        if (!linkStatus) {
            std::stringstream error;
            error << "can't link progam: \"" << getInfoLog() << "\""
                  << "\nvertex shader log: \"" << this->vertexShader.GetInfoLog() << "\""
                  << "\nfragment shader log: \"" << this->fragmentShader.GetInfoLog() << "\"";

            throw std::runtime_error(error.str());
        }
    }

    inline GLint getInfoLogLength() const noexcept {
        GLint length;
        glGetProgramiv(getId(), GL_INFO_LOG_LENGTH, &length);
        return length;
    }

    inline std::string getInfoLog() {
        size_t length = getInfoLogLength();
        if (length == 0) {
            return "";
        }
        std::string result(length, ' ');
        glGetProgramInfoLog(getId(), result.size(), nullptr, &result[0]);
        return result;
    }

    inline GLuint getId() const noexcept {
        return id.getValue();
    }

    inline void use() const noexcept {
        glUseProgram(id.getValue());
    }

    inline auto getUniformLocation(const GLchar *name) const {
        return glGetUniformLocation(getId(), name);
    }

    inline auto getAttribLocation(const GLchar *name) const {
        return glGetAttribLocation(getId(), name);
    }

    inline void setUniform(const GLchar *name, const mat4x4& matrix) noexcept {
        setUniform(getUniformLocation(name), matrix);
    }

    inline static void setUniform(GLint location, const mat4x4& matrix) noexcept {
        glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]);
    }

    inline static ShaderProgram Compile(const std::string& vertexCode, const std::string& fragmentCode) {
        return ShaderProgram{
            VertexShader::Compile(vertexCode),
            FragmentShader::Compile(fragmentCode),
        };
    }
};
