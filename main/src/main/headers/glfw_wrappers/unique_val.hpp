#pragma once

#include <utility>

template<class Value, class Deleter> 
class UniqueVal{
private:
    Value value;
    Deleter deleter;
public:
    explicit inline UniqueVal(Value value): value(value), deleter() {};

    inline UniqueVal(const UniqueVal&) = delete;

    inline UniqueVal& operator=(const UniqueVal&) = delete;

    inline UniqueVal(UniqueVal&& rhs): value(), deleter() {
        std::swap(value, rhs.value);
    };

    inline UniqueVal& operator=(UniqueVal&& rhs) noexcept {
        std::swap(value, rhs.value);
    }

    inline const Value& getValue() const noexcept{
        return value;
    }

    inline ~UniqueVal() {
        deleter(value);
    };
};
