#pragma once

#include "mygl.h"

#include "unique_val.hpp"



struct BufferDeleter {
    inline void operator()(GLuint id) {
        glDeleteBuffers(1, &id);
    }
};


template<int kTarget> 
class Buffer {
private:
    static_assert(kTarget == GL_ARRAY_BUFFER, "unsupported target");

    UniqueVal<GLuint, BufferDeleter> id;
public:
    inline Buffer(): id(genBuffer()) {
    }

    inline void bind() noexcept {
        glBindBuffer(kTarget, getId());
    }

    inline void setData(const void* data, GLsizeiptr size, GLenum usage) noexcept {
        glBufferData(kTarget, size, data, usage);
    }

    inline void setData(const float* data, GLsizeiptr size, GLenum usage) noexcept {
        setData(static_cast<const void*>(data), size * sizeof(float), usage);
    }

    inline GLuint getId() const noexcept {
        return id.getValue();
    }

    static inline GLuint genBuffer() noexcept {
        GLuint id; 
        glGenBuffers(1, &id);
        return id;
    }
};

using ArrayBuffer = Buffer<GL_ARRAY_BUFFER>;

