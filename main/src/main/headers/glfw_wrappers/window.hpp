#pragma once 

#include <iostream>
#include <memory>
#include <stdexcept>

#include <mygl.h>
#include <glm/vec2.hpp>

#include <util/math.hpp>



struct WindowDeleter {
    inline void operator()(GLFWwindow* window) {
        glfwDestroyWindow(window);
    }
};

class Window {   
private: 
    std::unique_ptr<GLFWwindow, WindowDeleter> window;
public: 
    inline Window(Vector2i size, const char *name) {
        GLFWwindow* ptr = glfwCreateWindow(size.x, size.y, name, NULL, NULL);
        if (!ptr) {
            throw new std::runtime_error("can't create window");
        };
        window.reset(ptr);
    }

    inline GLFWwindow* operator&() {
        return window.get();
    }
    
    inline bool isShouldClose() const {
        return glfwWindowShouldClose(window.get());
    }

    inline glm::vec2 getCursorPos() const {
        double xpos, ypos;
        glfwGetCursorPos(window.get(), &xpos, &ypos);
        return {static_cast<float>(xpos), static_cast<float>(ypos)};
    }

    inline Vector2i getSize() const {
        int w, h;
        glfwGetWindowSize(window.get(), &w, &h);
        return {w, h};
    }

    inline Vector2i getFramebufferSize() const {
        int w, h;
        glfwGetFramebufferSize(window.get(), &w, &h); 
        return {w, h}; 
    }

    inline void swapBuffers() noexcept {
        glfwSwapBuffers(window.get());
    }
};
