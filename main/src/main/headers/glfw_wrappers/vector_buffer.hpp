#pragma once 

#include <vector>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

class VectorBuffer {
private:
    std::vector<float> data;
public: 
    inline void push(float v) {
        data.push_back(v);
    }

    inline void push(glm::vec2 v) {
        push(v.x);
        push(v.y);
    }

    inline void push(glm::vec3 v) {
        push(v.x);
        push(v.y);
        push(v.z);
    }

    inline void push(glm::vec4 v) {
        push(v.x);
        push(v.y);
        push(v.z);
        push(v.w);
    }

    inline void clear() {
        data.clear();
    }

    const std::vector<float>& getData() const noexcept {
        return data;
    }
};