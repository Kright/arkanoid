#pragma once

#include <glm/vec2.hpp>
#include <util/math.hpp>

using glm::vec2;


struct PlatformRail {
    float y;
    float minX, maxX;
};


struct PlatformParams {
    vec2 size;
    float maxSpeed;
};


class Platform { 
private:
    PlatformRail rail;
    PlatformParams params;
    float posX = 0;

    inline float clampX(float x) {
        return clamp(x, rail.minX + params.size.x / 2, rail.maxX - params.size.x / 2);
    }
public:
    inline Platform(PlatformRail rail, PlatformParams params) : rail(rail), params(params) {};

    inline vec2 getSize() const {
        return params.size;
    }

    vec2 getCenter() const {
        return {posX, rail.y};
    }

    void update(float goalX, float dt) {
        posX = clampX(clamp(goalX, posX - params.maxSpeed * dt, posX + params.maxSpeed * dt));
    }
};
